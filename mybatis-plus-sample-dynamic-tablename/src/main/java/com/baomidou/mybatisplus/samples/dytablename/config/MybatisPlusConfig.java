package com.baomidou.mybatisplus.samples.dytablename.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.DynamicTableNameInnerInterceptor;
import com.baomidou.mybatisplus.samples.dytablename.interceptor.MyInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author miemie
 * @since 2018-08-10
 */
@Configuration
@MapperScan("com.baomidou.mybatisplus.samples.dytablename.mapper")
public class MybatisPlusConfig {

    @Resource
    DynamicTableNameInnerInterceptor userDynamicTableNameInnerInterceptor;
    @Resource
    MyInnerInterceptor myInnerInterceptor;

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(userDynamicTableNameInnerInterceptor);
        interceptor.addInnerInterceptor(myInnerInterceptor);
        return interceptor;
    }
}
