package com.baomidou.mybatisplus.samples.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudApplication {

    /**
     * CRUD  丰富的案例
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(CrudApplication.class, args);
    }
}

