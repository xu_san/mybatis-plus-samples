package com.baomidou.mybatisplus.samples.enums.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

/**
 * <p>
 * </p>
 *
 * @author yuxiaobin
 * @date 2018/8/31
 */
@Getter
public enum GradeEnum {

    /*
     * 小学
     */
    PRIMARY("1", "小学"),
    SECONDORY("2", "中学"),
    HIGH("3", "高中");

    private final String code;
    @EnumValue
    private final String descp;
    GradeEnum(String code, String descp) {
        this.code = code;
        this.descp = descp;
    }

}
