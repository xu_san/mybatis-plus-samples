package com.baomidou.mybatisplus.samples.sequence.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class User {

    /*
     * INPUT 是用户自己手工输入ID，如果不想手工输入，可以@KeySequence，然后在配置对应生成器：H2KeyGenerator
     */
//    @TableId(value = "id", type = IdType.INPUT)

    /*
     * ASSIGN_ID，是自动分配Long类型的ID，可以通过实现IdentifierGenerator接口，自定义
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    private String name;
    private Integer age;
    private String email;
}
