package com.baomidou.mybatisplus.samples.sequence.config;

import cn.hutool.core.lang.generator.SnowflakeGenerator;
import cn.hutool.core.lang.generator.UUIDGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.springframework.stereotype.Component;

@Component
public class MyKeyGenerator implements IdentifierGenerator {


    @Override
    public Number nextId(Object entity) {
        /*
         * 得到基础的实现后，就可以根据预定，自定义实现ID的
         */
        System.out.println(entity.getClass().getSimpleName());
        SnowflakeGenerator snowflakeGenerator = new SnowflakeGenerator();
        return snowflakeGenerator.next();
    }

    @Override
    public String nextUUID(Object entity) {
        UUIDGenerator uuidGenerator = new UUIDGenerator();
        return uuidGenerator.next();
    }
}
