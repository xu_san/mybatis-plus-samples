package com.baomidou.mybatisplus.samples.tenant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TenantApplication {

    /**
     * 其实不需要在应用层划分和实现多组用户，这个概念是数据库层的..后面即便是做了分库分表，对应用层也是要求尽量透明、无侵入式
     *
     * @param args 入参
     */
    public static void main(String[] args) {
        SpringApplication.run(TenantApplication.class, args);
    }
}

