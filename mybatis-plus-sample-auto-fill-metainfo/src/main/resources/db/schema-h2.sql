-- noinspection SqlNoDataSourceInspectionForFile

DROP TABLE IF EXISTS user;

CREATE TABLE user
(
    id           BIGINT(20)  NOT NULL COMMENT '主键ID',
    name         VARCHAR(30) NULL DEFAULT NULL COMMENT '姓名',
    age          INT(11)     NULL DEFAULT NULL COMMENT '年龄',
    email        VARCHAR(50) NULL DEFAULT NULL COMMENT '邮箱',
    operator     VARCHAR(30) NULL DEFAULT NULL COMMENT '操作员',
    created_time TIMESTAMP   NULL NULL COMMENT '创建时间',
    updated_time TIMESTAMP   NULL NULL COMMENT '更新时间',
    PRIMARY KEY (id)
);
