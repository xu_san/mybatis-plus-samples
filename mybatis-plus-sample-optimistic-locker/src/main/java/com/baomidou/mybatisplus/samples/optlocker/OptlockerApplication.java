package com.baomidou.mybatisplus.samples.optlocker;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.baomidou.mybatisplus.samples.*.mapper")
public class OptlockerApplication {

    /**
     * 简述一下拦截器作用：
     * 1、update的时候，Version字段是作为where 条件的；
     * 2、每次update，都需要再代码中修改Version的值；
     * 3、由数据库提供并发控制（数据库自己的行锁）；
     * 4、正常情况下，一条记录不会被无限制update，所以基础的int类型即可；
     *
     * @param args 入参
     */
    public static void main(String[] args) {
        SpringApplication.run(OptlockerApplication.class, args);
    }
}
