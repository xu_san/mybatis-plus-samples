package com.baomidou.mybatisplus.samples.wrapper;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.baomidou.mybatisplus.samples.wrapper.mapper")
public class WrapperApplication {

    /**
     * 结合之前看到的源码分析：Wrapper 就是属于MyBatis自己抽象的更高级的一个类，可以动态的获取Entity的属性信息，来解析sql
     *
     * @param args 入参
     */
    public static void main(String[] args) {
        SpringApplication.run(WrapperApplication.class, args);
    }

}
