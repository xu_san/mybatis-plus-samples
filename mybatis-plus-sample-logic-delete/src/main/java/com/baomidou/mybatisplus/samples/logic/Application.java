package com.baomidou.mybatisplus.samples.logic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.baomidou.mybatisplus.samples.logic.mapper")
public class Application {

    /**
     * 逻辑删除的规则太"霸道了"，有时候查不出来，不确定是真的插入，还是确实没数据
     *
     * @param args 入参
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
