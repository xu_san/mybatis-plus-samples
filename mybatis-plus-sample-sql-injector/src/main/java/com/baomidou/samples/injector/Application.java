package com.baomidou.samples.injector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动器
 *
 * @author nieqiurong 2018/8/11 20:32.
 */
@SpringBootApplication
public class Application {

    /**
     * 如果觉得MyBatis Plus提供的方法不够，需要自定义全局方法的话，可以参考这个项目，具体步骤为：
     * 1、自己继承AbstractMethod这个抽象类；
     * 2、实现injectMappedStatement 方法；
     * 3、自定义实现DefaultSqlInjector 类，把之前定义的方法添加进去
     *
     * @param args 入参
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
